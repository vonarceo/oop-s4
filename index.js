console.log("Von Arceo")

class Student {
	// constructor keyword allows us to pass arguments when we instantiate objects from classes
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		this.gradeAve = undefined;
		
		this.passed = undefined;
		this.passedWithHonors = undefined;

		// ACTIVITY 1 Item 1
		// check first if the array has 4 elements
		if(grades.length === 4){
		    if(grades.every(grade => grade >= 0 && grade <= 100)){
		        this.grades = grades;
		    } else {
		        this.grades = undefined;
		    }
		} else {
		    this.grades = undefined;
		}
	}
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout(){
		console.log(`${this.email} has logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve(){
	    let sum = 0;
	    this.grades.forEach(grade => sum = sum + grade);
	    // update the gradeAve property
	    this.gradeAve = sum/4;
	    // returns the object
	    return this;
	}
	// ACTIVITY 1 Item 2
	willPass() {
	    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
	    return this;
	}
	// ACTIVITY 1 Item 2
	willPassWithHonors() {
	    if (this.passed) {
	        if (this.gradeAve >= 90) {
	            this.passedWithHonors = true;
	        } else {
	            this.passedWithHonors = false;
	        }
	    } else {
	        this.passedWithHonors = false;
	    }
	    return this;
	}
}


// Section Class to store multiple student classes;

class Section{
    constructor(name){
        this.name = name;
        this.students = [];
		this.honorStudents = undefined;
		this.honorPercentage = undefined;
    }

    addStudent(name, email, grades){
        this.students.push(new Student(name, email, grades));
        
        return this;
    }
	countHonorStudents(){

		// passwithHonors property will only be updated when willpaswithhonor is invoke.
		// will passwithHonor() depends on the execution of willpass()
		//will() relies on the gradeave to be computed by the computeave();



		let count = 0;
		this.students.forEach(student => {
			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++;
			}
		})
		this.honorStudents = count;
		return this;
	}
	computeHonorsPercentage(){

			let honor = this.honorStudents;
			let totalStudents = this.students.length;
		
			this.honorPercentage = (100 * honor) / totalStudents;

			return this;

		
		
	}

}

const section1A = new Section("section1A");

section1A.addStudent("John", "john@email.com", [89,84,78,88])
section1A.addStudent("Joe", "joe@email.com", [78,82,79,85])
section1A.addStudent("Jane", "jane@email.com", [87,89,91,93])
section1A.addStudent("Jessie", "jessie@email.com", [91,89,92,93])

/* //-----------for testing------------//

//instantiate new Grade object
const grade1 = new Grade(1);

//add sections to this grade level
grade1.addSection('section1A');
grade1.addSection('section1B');
grade1.addSection('section1C');
grade1.addSection('section1D');

//save sections of this grade level as constants
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

//populate the sections with students
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);
 */

class Grade{
	constructor(gradelevel){
		this.gradelevel = gradelevel;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;
	}

	addSection(sectionName){
		this.sections.push(new Section(sectionName))

		return this;
	}

	countStudents(){
		let count = 0
        this.sections.forEach(section => {
            count += section.students.length
        })
        this.totalStudents = count
        return this

	}

	countHonorStudents(){
        for(let section of this.sections){
            for(let student of section.students){
                student.computeAve().willPass().willPassWithHonors();
                student.passedWithHonors?  this.totalHonorStudents++: this.totalHonorStudents
            };            
        };
        return this;
    }

	computeBatchAve(){
        countStudents();
        let totalGrades = 0;

        for(let section of this.sections){
            for(let student of section.students){
                totalGrades += student.computeAve().gradeAve;    
            };            
        };
        this.batchAveGrade = totalGrades /  this.totalStudents;
        return this;
    }

	getBatchMinGrade(){
        this.batchMinGrade = 100;
        for(let section of this.sections){
            for(let student of section.students){
                for(let grade of student.grades)
                if(grade < this.batchMinGrade) this.batchMinGrade = grade;
            };            
        };
        return this;
    }

	getBatchMaxGrade(){
        this.batchMaxGrade = 0;
        for(let section of this.sections){
            for(let student of section.students){
                for(let grade of student.grades)
                if(grade > this.batchMaxGrade) this.batchMaxGrade = grade;
            };            
        };
        return this;
    }










}

const grade1 = new Grade(1)
    grade1.addSection("section1A")
    grade1.sections.find(section => section.name === "section1A").addStudent("John","john@mail.com",[89,84,78,88])
    grade1.sections.find(section => section.name === "section1A").addStudent("Jake","jake@mail.com",[80,86,93,76])
    grade1.sections.find(section => section.name === "section1A").addStudent("Mike","mike@mail.com",[84,89,88,88])
    grade1.sections.find(section => section.name === "section1A").addStudent("Susie","susie@mail.com",[98,99,98,87])

    grade1.addSection("section1B")
    grade1.sections.find(section => section.name === "section1B").addStudent("Joe","joe@mail.com",[93,82,79,90])
    grade1.sections.find(section => section.name === "section1B").addStudent("Mary","mary@mail.com",[78,78,79,85])
    grade1.sections.find(section => section.name === "section1B").addStudent("Jane","jane@mail.com",[87,89,91,93])
    grade1.sections.find(section => section.name === "section1B").addStudent("Jonas","jonas@mail.com",[78,82,79,85])

    grade1.addSection("section1C")
    grade1.sections.find(section => section.name === "section1C").addStudent("Chito","chito@mail.com",[85,85,90,90])
    grade1.sections.find(section => section.name === "section1C").addStudent("Kiko","francism@mail.com",[87,83,91,90])
    grade1.sections.find(section => section.name === "section1C").addStudent("Gloc-9","walangapelyido@mail.com",[85,85,85,85])
    grade1.sections.find(section => section.name === "section1C").addStudent("Rico","rico@mail.com",[87,89,91,93])

    grade1.addSection("section1D")
    grade1.sections.find(section => section.name === "section1D").addStudent("Eheads","eheads@mail.com",[83,85,85,85])
    grade1.sections.find(section => section.name === "section1D").addStudent("Parokya","parokya@mail.com",[90,89,95,95])
    grade1.sections.find(section => section.name === "section1D").addStudent("Kamikazee","kamikazee@mail.com",[83,85,85,85])
    grade1.sections.find(section => section.name === "section1D").addStudent("Orange&Lemons","orangelemons@mail.com",[83,85,85,85])


